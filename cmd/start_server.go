package main

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/douglasmanicka/trading-study/server"
)

const configEnvFile = "config.env"

func init() {
	err := godotenv.Load(configEnvFile)
	if err != nil {
		log.Fatalf("Error loading config.env file")
	}
}

func main() {
	srv, err := server.New()
	if err != nil {
		logrus.Error("Cannot create  a Server", err)
		panic(err)
	}
	srv.Run(":8085")

}
