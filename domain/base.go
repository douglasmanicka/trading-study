package domain

import (
	"database/sql"
	"time"

	"gorm.io/gorm"
)

type BaseModel struct {
	// the time the object has been created
	CreatedAt time.Time `json:"created_at"`
	// the time the last update happened to the object
	UpdatedAt sql.NullTime `json:"updated_at"`
	// if the cause has been soft deleted
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}
