module gitlab.com/douglasmanicka/trading-study

go 1.16

require (
	github.com/aviddiviner/gin-limit v0.0.0-20170918012823-43b5f79762c1
	github.com/gin-gonic/gin v1.7.1
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.8.1
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.9
)
