package server

import (
	"fmt"
	"os"

	"github.com/gin-gonic/gin"

	"github.com/sirupsen/logrus"
	"gitlab.com/douglasmanicka/trading-study/domain"
	util "gitlab.com/douglasmanicka/trading-study/utils"

	limit "github.com/aviddiviner/gin-limit"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Server struct {
	db     *gorm.DB
	router *gin.Engine
}

// Build Routers
func (srv *Server) buildRouters() {

	srv.router = gin.Default()

	apiV1 := srv.router.Group("/api/v1")

	//rate limit
	apiV1.Use(limit.MaxAllowed(20))

	//log some request information
	apiV1.Use(util.LogMiddleware())

	apiV1.POST("/offer", srv.CreateOffer())
	apiV1.PUT("/offer", srv.EditOffer())
	apiV1.DELETE("/offer", srv.DeleteOffer())
	apiV1.GET("/offer", srv.GetOffer())
	apiV1.GET("/offers", srv.GetOffers())
	apiV1.GET("/orders", srv.GetPossibleOrders())

}

// Open DB Connection
func (srv *Server) openDBConnection() error {
	var err error
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_DBNAME"), os.Getenv("DB_PORT"))

	logrus.Infof("Opening Database connection")
	srv.db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		logrus.Error(err)
		return err
	}

	return nil
}

// Migrate DB
func (srv *Server) MigrateDB() error {

	logrus.Infof("Migrate tables")
	if err := srv.db.AutoMigrate(
		&domain.Offer{},
	); err != nil {
		return err
	}

	return nil
}

// Create new Server
func New() (*Server, error) {

	srv := &Server{}

	if err := srv.openDBConnection(); err != nil {
		return nil, err
	}

	logrus.Infof("Database migration")
	if err := srv.MigrateDB(); err != nil {
		return nil, err
	}

	srv.buildRouters()

	if srv == nil {
		panic("server struct is nil")
	}

	return srv, nil
}

// Run Server
func (srv *Server) Run(port string) {

	logrus.Infof("Starting  Backend Server on port: %s", port)
	if err := srv.router.Run(port); err != nil {
		logrus.Error("Cannot start server", err)
	}

}
