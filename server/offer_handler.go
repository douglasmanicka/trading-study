package server

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	"github.com/shopspring/decimal"
	"gitlab.com/douglasmanicka/trading-study/domain"
	"gorm.io/gorm"

	//"gitlab.com/douglasmanicka/trading-study/utils/database"
	resp "gitlab.com/douglasmanicka/trading-study/utils/responses"
)

func (srv *Server) CreateOffer() gin.HandlerFunc {
	type Params struct {
		Ticket    string `json:"ticket_name"`
		SellPrice string `json:"sell_price"`
		BuyPrice  string `json:"buy_price"`
		Quantity  string `json:"quantity"`
		Status    string `json:"status"`
	}

	return func(ctx *gin.Context) {
		var params Params
		offer := domain.Offer{}

		if err := ctx.ShouldBindJSON(&params); err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		if strings.TrimSpace(params.Ticket) == "" || strings.TrimSpace(params.SellPrice) == "" || strings.TrimSpace(params.Quantity) == "" || strings.TrimSpace(params.Status) == "" {
			resp.RespondWithErrorMessage(ctx, "Ticket or SellPrice or Quantity or Sttus values are empty or not specified.These parameters are mandatory")
			return
		}

		sellPrice, err := decimal.NewFromString(params.SellPrice)
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}
		buyPrice, err := decimal.NewFromString(params.BuyPrice)
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}
		quantity, err := decimal.NewFromString(params.Quantity)
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		offer.Ticket = params.Ticket
		offer.SellPrice = sellPrice
		offer.BuyPrice = buyPrice
		offer.Quantity = quantity
		offer.Status = params.Status
		offer.CreatedAt = time.Now()

		// if errDb := srv.db.Create(&offer).Error; errDb != nil {
		// 	resp.RespondWithError(ctx, errDb)
		// 	return
		// }

		if err := srv.db.Transaction(func(tx *gorm.DB) error {
			if errDb := tx.Create(&offer).Error; errDb != nil {
				return err
			}
			return nil
		}); err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		resp.RespondWithData(ctx, &offer)

	}
}

func (srv *Server) EditOffer() gin.HandlerFunc {
	type Params struct {
		OfferID   string `json:"offer_id"`
		Ticket    string `json:"ticket_name"`
		SellPrice string `json:"sell_price"`
		BuyPrice  string `json:"buy_price"`
		Quantity  string `json:"quantity"`
		Status    string `json:"status"`
	}

	return func(ctx *gin.Context) {
		var params Params

		if err := ctx.ShouldBindJSON(&params); err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		if strings.TrimSpace(params.OfferID) == "" {
			resp.RespondWithErrorMessage(ctx, "Offer Id is mandatory")
			return
		}

		if strings.TrimSpace(params.Ticket) == "" && strings.TrimSpace(params.SellPrice) == "" && strings.TrimSpace(params.BuyPrice) == "" && strings.TrimSpace(params.Quantity) == "" && strings.TrimSpace(params.Status) == "" {
			resp.RespondWithErrorMessage(ctx, "All values are empty or not specified. You need some value to update")
			return
		}

		sellPrice, err := decimal.NewFromString(params.SellPrice)
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		buyPrice, err := decimal.NewFromString(params.BuyPrice)
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		quantity, err := decimal.NewFromString(params.Quantity)
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		offer := domain.Offer{
			Ticket:    params.Ticket,
			SellPrice: sellPrice,
			BuyPrice:  buyPrice,
			Quantity:  quantity,
			Status:    params.Status,
		}

		if err := srv.db.Transaction(func(tx *gorm.DB) error {
			if errDb := tx.Model(&offer).Where("id = ?", uuid.FromStringOrNil(params.OfferID)).Updates(&offer).Error; errDb != nil {
				return err
			}
			return nil

		}); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				resp.RespondWithErrorMessage(ctx, fmt.Sprintf("offerId = %s not found", params.OfferID))
			} else {
				resp.RespondWithError(ctx, err)
			}
			return
		}

		resp.RespondWithData(ctx, fmt.Sprintf("offerId = %s  succefuly updated", params.OfferID))

	}
}

func (srv *Server) DeleteOffer() gin.HandlerFunc {
	type Params struct {
		OfferID string `json:"offer_id"`
	}

	return func(ctx *gin.Context) {
		var params Params

		if err := ctx.ShouldBindJSON(&params); err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		if err := srv.db.Transaction(func(tx *gorm.DB) error {
			if err := tx.Delete(&domain.Offer{}, uuid.FromStringOrNil(params.OfferID)).Error; err != nil {
				return err
			}
			return nil
		}); err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		resp.RespondWithData(ctx, fmt.Sprintf("offerId = %s  succefuly deleted", params.OfferID))

	}
}

func (srv *Server) GetOffer() gin.HandlerFunc {
	type Params struct {
		OfferID string `json:"offer_id"`
	}

	return func(ctx *gin.Context) {
		var params Params
		offer := domain.Offer{}

		if err := ctx.ShouldBindJSON(&params); err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		if strings.TrimSpace(params.OfferID) == "" {
			resp.RespondWithErrorMessage(ctx, "Offer Id is mandatory")
			return
		}

		if err := srv.db.Transaction(func(tx *gorm.DB) error {
			if errDb := tx.Table("offers").Where("id = ?", params.OfferID).First(&offer).Error; errDb != nil {
				return errDb
			}
			return nil

		}); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				resp.RespondWithErrorMessage(ctx, fmt.Sprintf("offerId = %s not found", params.OfferID))
			} else {
				resp.RespondWithError(ctx, err)
			}
			return
		}

		resp.RespondWithData(ctx, &offer)

	}
}

func (srv *Server) GetOffers() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		offers := []domain.Offer{}

		if err := srv.db.Transaction(func(tx *gorm.DB) error {
			if errDb := tx.Order("sell_price desc").Find(&offers).Error; errDb != nil {
				return errDb
			}
			return nil

		}); err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		resp.RespondWithData(ctx, &offers)
	}
}

// basically orders are 2 offers that cancel each other
// Idea: compare all offers with all offers.
// find zero or more offers than sales price <= purchase price
// and returns an matrix of orders
//
// note: I haven't made a mass of data to test
// this dont work correctly yet, it may need adjustments and more tests
func (srv *Server) GetPossibleOrders() gin.HandlerFunc {
	return func(ctx *gin.Context) {

		offersBuyAsc := []domain.Offer{}
		offersSellDesc := []domain.Offer{}

		err := srv.db.Order("buy_price asc").Find(&offersBuyAsc).Error
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		err = srv.db.Order("sell_price desc").Find(&offersSellDesc).Error
		if err != nil {
			resp.RespondWithError(ctx, err)
			return
		}

		//match  offers/orders
		offerMatch := []domain.Offer{}
		matchOrders := [][]domain.Offer{}

		for _, v := range offersBuyAsc {
			for _, c := range offersSellDesc {
				if v.SellPrice.LessThanOrEqual(c.BuyPrice) {
					offerMatch = append(offerMatch, v, c)
					matchOrders = append(matchOrders, offerMatch)
				}

			}

		}

		resp.RespondWithData(ctx, matchOrders)

	}
}
