package resp

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func RespondWithError(context *gin.Context, err error) {
	context.JSON(http.StatusInternalServerError, context.Error(err))
	return
}

func RespondWithErrorMessage(context *gin.Context, message string) {
	context.JSON(http.StatusInternalServerError, message)
	return
}

func RespondWithData(ctx *gin.Context, data interface{}) {
	ctx.JSON(http.StatusOK, data)
}

func RespondSuccess(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
}
