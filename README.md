# trading-study

A simple trading project for study

- To run:
  - Clone the project
  
  - Create a postgres database

  - Create file config.env:
    - DB_HOST=SOME_POSTGRES_HOST
    - DB_USER=SOME_POSTGRES_USER
    - DB_PASSWORD=SOME_POSTGRES_PASSWORD
    - DB_DBNAME=SOME_POSTGRES_DATABASE_NAME
    - DB_PORT=SOME_POSTGRES_DATABASE_PORT

  - go run cmd/start_server.go
    